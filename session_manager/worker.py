import time
import logging
from threading import Thread
from fsm.fsm_base import *
from yce.yce import *
from ml.text_clustering import *



class SessionWorker(Thread):
    """Сессионный воркер. Обрабатывает сообщения из очереди"""
    def __init__(self, name, sm):
        super().__init__(name="SessionWorker_{}".format(str(name)))
        self.name = name
        self.sm = sm
        self.logger = logging.getLogger('SessionWorker {}'.format(self.name))
        self.logger.debug("Worker created")
        self.uid = None
        pass

    def run(self):
        while True:
            try:
                task = self.sm.incoming_queue.get(block=True)
                self.uid = task["id"]
                with self.sm.lock:
                    if task["id"] not in self.sm.sessions:
                        self.logger.debug("Creating Session object for id {}".format(task["id"]))
                        self.sm.sessions[task["id"]] = self.sm.session_object(self.sm.full_cfg, task["id"], self.sm.outcoming_queue)

                with self.sm.sessions[task["id"]].lock:
                    self.logger.debug("Call update for Session object {}".format(task["id"]))
                    self.sm.sessions[task["id"]].update(task["data"])

                self.sm.tasks_queue.task_done()
                self.logger.info("Id {}: task done".format(task["id"]))

            except (FSMException, TCException, YCEException) as e:
                self.logger.error(e, exc_info=True)
                if hasattr(e, "user_message"):
                    self.sm.send(self.uid, e.user_message)
                if hasattr(e, "type") and e.type == "critical":
                    self.sm.send(None, e.__str__())

            except Exception as e:
                self.logger.critical(e, exc_info=True)
                if hasattr(e, "type") and e.type == "critical":
                    self.sm.send(None, e.__str__())

            finally:
                time.sleep(0.1)
