import time
import logging
from threading import Thread
from threading import RLock

from session_manager.worker import SessionWorker


class SessionManager(Thread):
    """Менеджер сессий. Предоставляет очередь сообщений для обработки, запускает воркеры,
    в остальное время контроллирует(прибивает) сессии"""

    def __init__(self, config, outcoming_queue, incoming_queue, session_object):
        super().__init__(name="SessionManager")
        self.logger = logging.getLogger("SessionManager")
        self.full_cfg = config

        try:
            self.module_cfg = config["session_manager"]
            self.session_timeout = self.module_cfg["session_timeout"]
            self.workers_threads = self.module_cfg["worker_threads"]
        except KeyError as e:
            raise e

        self.sessions = {}
        self.workers = {}
        self.incoming_queue = incoming_queue
        self.outcoming_queue = outcoming_queue
        self.lock = RLock()
        self.session_object = session_object

        for _ in range(self.workers_threads):
            worker = SessionWorker(_, self)
            worker.start()
            self.workers[_] = worker
        self.logger.debug("Session manager created")

    def run(self):
        while True:
            curr_time = time.time()
            for sid in list(self.sessions):
                if curr_time - self.sessions[sid].last_action >= self.session_timeout:
                    del self.sessions[sid]

            for key in self.workers:
                if self.workers[key].isAlive() is not True:
                    name = self.workers[key].name
                    self.logger.error("Worker{} is not alive! Starting the new worker".format(name))
                    del self.workers[key]
                    w = SessionWorker(name, self)
                    w.start()
                    self.workers[key] = w
            time.sleep(1)

    def send(self, uid, msg):
        if uid is None:
            uid = self.module_cfg["admin"]
        self.outcoming_queue.put({"chat_id": uid, "text": msg})




