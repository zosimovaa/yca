from fsm.fsm_telegram import FSMTelegram
from session_manager.test.fsm_test_config import fsm as cfg

from session_manager.manager import SessionManager

import logging
import queue
import sys
task_queue = queue.Queue()



class FSMTest(FSMTelegram):
    def __init__(self, config, chat_id, message_queue):
        super().__init__(config, chat_id, message_queue)

    def cmd_state1(self, data):
        self.logger.debug("cmd_state1 was called")
        self.logger.debug("Data: ".format(data))
        pass

    def cmd_state3(self, data):
        self.logger.debug("cmd_state3 was called")
        self.logger.debug("Data: ".format(data))
        pass



def msg(m):
    """Эмуляция очереди для отправки сообщений"""
    print(m)



def main():
    tq = queue.Queue()
    mq = queue.Queue()


    s = SessionManager(tq, mq, FSMTest, cfg)
    s.start()

    s.tasks_queue.put({
        "id": 111,
        "data": "state1 message 111"
    })

    s.tasks_queue.put({
        "id": 111,
        "data": "state1 message 222"
    })

    s.tasks_queue.put({
        "id": 111,
        "data": "state2 message 333"
    })

    s.tasks_queue.put({
        "id": 111,
        "data": "state3 message 444"
    })







if __name__ == "__main__":
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG,
                        format='%(asctime)-12s : %(name)-15s : %(levelname)-8s %(message)s')
    main()



