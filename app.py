import os
import yaml
import queue
import logging
import logging.config

from telegram import Telegram
from telegram import filters
from session_manager import SessionManager
from fsm import FSMYoutube


def main(cfg):
    logging.info("Application started")
    outcoming_queue = queue.Queue()              #msgs исходящие сообщения,обрабатываются сендером
    incoming_queue = queue.Queue()              #tsks sвходящие собщения, обрабатываются воркерами сессионного менеджера

    bot = Telegram(cfg, outcoming_queue, incoming_queue, filters)
    session_manager = SessionManager(cfg, outcoming_queue, incoming_queue, FSMYoutube)

    bot.start()
    session_manager.start()

    bot.send(cfg["admin_chat"],
             cfg["start_message"].replace("<<version>>", cfg["version"]))


if __name__ == "__main__":
    config_env = os.environ.get('YCA_ENV', None)

    if config_env == "dev":
        config_file_name = "config_dev.yml"
    elif config_env == "prod":
        config_file_name = "config_prod.yml"
    else:
        raise ValueError('Incorrect environment variable')

    with open(config_file_name, 'r') as config_file:
        config = yaml.safe_load(config_file.read())
        logging.config.dictConfig(config["logger"])
    main(config)
