import re
import json
from threading import RLock

#from config import google
from fsm.fsm_telegram import FSMTelegram
from yce.yce import YoutubeCommentExtractor
from ml.text_clustering import TxtCluster


class FSMYoutube(FSMTelegram):
    def __init__(self, config, chat_id, outcoming_queue):
        super().__init__(config, chat_id, outcoming_queue)

        self.c = config
        self.config_yce = config["yce"]

        self.lock = RLock()
        self.vid = None
        self.comments = None
        self.yca = YoutubeCommentExtractor(self.config_yce)

        self.set_comments_cnt = 1000
        self.set_clstr_cnt = 10
        self.set_clstr_words = 7
        return

    def cmd_yt_stage1(self, data=None):
        """Обработка команды, скачивание комментариев. Уведомление о статусе. Переход в стейт yt_1 где формируется меню действий"""
        self.logger.debug("cmd_yt_stage1 data: {}".format(data))
        self.vid = data
        self.send("Downloading comments...")
        self.comments = self.yca.get_video_comments(self.vid, self.set_comments_cnt)
        self.send("Done!")
        #TODO сделать обработку исключений
        self.set_state("yt_1").update()

        return

    def cmd_yt_stage2(self, data=None):
        """Анализ тем. По итогу возврат в yt_1"""
        self.logger.debug("cmd_yt_stage2")

        cluster = TxtCluster()
        msg = cluster.processing(self.comments, self.set_clstr_cnt, self.set_clstr_words)
        self.send(msg)

        self.set_state("yt_1").update()
        return

    def cmd_yt_stage3(self, data=None):
        """Анализ тональности. По итогу возврат в yt_1"""
        self.logger.debug("cmd_yt_stage3")
        self.set_state("yt_1").update()
        #raise NotImplementedError
        return

    def cmd_yt_stage4_1(self, data=None):
        #todo сделать перезаливку комментариев при изменении количества (или дозалдивку???)
        #todo сделать кнопку назад
        min_value = 1
        max_value = 50000
        """Настройка количества комментариев для скачки. По итогу возврат в yt_4 - Настройки """
        self.logger.debug("cmd_yt_stage4_1")
        if data is None:
            msg = "Текущее количество коментариев для скачивания: {0}. \nВведите новое значение от {1} до {2}:".format(self.set_comments_cnt, min_value, max_value)
            self.send(msg, [["Назад", "yt_4"]])
        else:
            try:
                cnt = int(data)
                if (cnt >= min_value) and (cnt <= max_value):
                    self.set_clstr_words = cnt
                    self.send("Done!")
                    self.set_state("yt_4").update()
                else:
                    self.send("Пожалуйста введите значение от {0} до {1}".format(min_value, max_value))
            except ValueError:
                self.send("Пожалуйста введите значение от {0} до {1}".format(min_value, max_value))
        return

    def cmd_yt_stage4_2(self, data=None):
        min_value = 1
        max_value = 30
        """Настройка количества кластеров. По итогу возврат в yt_4 - Настройки """
        self.logger.debug("cmd_yt_stage4_2")
        if data is None:
            self.send("Текущее количество кластеров: {0}. \nВведите новое значение от {1} до {2}:".format(self.set_clstr_cnt, min_value, max_value), [["Назад", "yt_4"]])
        else:
            try:
                cnt = int(data)
                if (cnt >= min_value) and (cnt <= max_value):
                    self.set_clstr_words = cnt
                    self.send("Done!")
                    self.set_state("yt_4").update()
                else:
                    self.send("Пожалуйста введите значение от {0} до {1}".format(min_value, max_value))
            except ValueError:
                self.send("Пожалуйста введите значение от {0} до {1}".format(min_value, max_value))
        return

    def cmd_yt_stage4_3(self, data=None):
        min_value = 1
        max_value = 20
        """Настройка количества слов в кластере. По итогу возврат в yt_4 - Настройки """
        self.logger.debug("cmd_yt_stage4_3")
        if data is None:
            self.send("Текущее количество слов в кластере: {0}. \nВведите новое значение от {1} до {2}:".format(self.set_clstr_words, min_value, max_value), [["Назад", "yt_4"]])
        else:
            try:
                cnt = int(data)
                if (cnt >= min_value) and (cnt <= max_value):
                    self.set_clstr_words = cnt
                    self.send("Done!")
                    self.set_state("yt_4").update()
                else:
                    self.send("Пожалуйста введите значение от {0} до {1}".format(min_value, max_value))
            except ValueError:
                self.send("Пожалуйста введите значение от {0} до {1}".format(min_value, max_value))
        return

