import logging


class FSMException(Exception):
    """Базовое исключение конечного автомата"""
    def __init__(self, msg):
        super().__init__(msg)
        self.user_message = "Произошла ошибка. Повторите попытку позже."


class TransitionDoesNotAllowedError(FSMException):
    """Ошибка - переход на стейт не разрешен"""
    def __init__(self, new_state, current_state, allowed_transitions):
        msg = "Transition {0} is not allowed. Current state: {1}".format(new_state, current_state)
        super().__init__(msg)
        self.new_state = new_state
        self.current_state = current_state
        self.allowed_transitions = allowed_transitions
        self.user_message = "Переход на стейт {} не разрешен. Введите корректную команду или начните заново - /start".format(self.new_state)


class HandlerDoesNotExistError(FSMException):
    """Ошибка - отсутствует обработчик для стейта"""
    def __init__(self, handler):
        msg = "Handler {0} does not exists".format(handler)
        super().__init__(msg)
        self.handler = handler
        self.user_message = "Произошла ошибка. Администратор уведомлен о проблеме. Повторите попытку позже."


class BadConfigError(FSMException):
    """Ошибки при парсинге конфига"""
    def __init__(self, e):
        msg = "Bad config {}".format(e)
        super().__init__(msg)
        self.original_exception = e
        self.user_message = "Произошла ошибка. Администратор уведомлен о проблеме. Повторите попытку позже."


class FSMBase:
    """ Finite State Machine - конечный автомат """
    wrong_state_msg = "неправильная команда или сдохла сессия"

    def __init__(self, config, uid):
        self.logger = logging.getLogger("FSM|{0}".format(uid))
        self.uid = uid
        try:
            self.c = config["fsm_youtube2"]
            self.init_state = self.c["init_state"]
            self.state = self.c["init_state"]
            self.transitions = self.c["transitions"]
        except KeyError:
            raise BadConfigError(KeyError)

        self.current_state = self.transitions[self.state]
        self.allowed_transitions = None
        self.logger.debug("Session created")

    def __del__(self):
        self.logger.debug("Session deleted")

    def at_parser(self):
        """ Парсер доступных переходов. Собирает переходы из разных полей стейта, зависит от конкретной реализации"""
        at = set()
        at.add(self.state)
        at.add(self.init_state)

        if "data" in self.current_state and "next" in self.current_state["data"]:
            at.add(self.current_state["data"]["next"])

        if "transitions" in self.current_state:
            at = at.union(set(self.current_state["transitions"]))

        self.logger.debug("Allowed transitions: {0}".format(at))
        return at

    def set_state(self, new_state):
        """ Процедура установки нового стейта """
        self.logger.debug("Trying to set the new state: {0}".format(new_state))
        at = self.at_parser()
        #TODO Сделать обработку отсутствующего стейта в конфиге

        if new_state not in self.transitions:
            raise BadConfigError(new_state)

        if new_state in at:
            self.state = new_state
            self.current_state = self.transitions[self.state]
            self.logger.debug("New state for chat id {0} - {1}".format(self.uid, new_state))
            return self
        else:
            self.send(self.wrong_state_msg)
            raise TransitionDoesNotAllowedError(new_state, self.current_state, at)

    def action(self, state=None, data=None):
        self.logger.debug("Update state: {0}, data: {1}".format(state, data))
        if state is not None:
            self.set_state(state)

        if "handler" in self.current_state:
            if hasattr(self, self.current_state["handler"]):
                getattr(self, self.current_state["handler"])(data)  # Уходим в хендлер

            else:
                raise HandlerDoesNotExistError(self.current_state["handler"])

    def message(self, data):
        if "data" in self.current_state:
            if "text" in self.current_state["data"]:
                self.send(self.current_state["data"]["text"])

            if "next" in self.current_state["data"]:
                self.action(self.current_state["data"]["next"], data)

    def send(self, msg):
        print(msg)
