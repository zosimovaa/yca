import re
import json
import time
from fsm.fsm_base import FSMBase

COMMAND_PATTERN = "\/?[\w-]*"  #"(\/?\w+)(?:\s)(.*)"


class FSMTelegram(FSMBase):
    def __init__(self, config, uid, outcoming_queue):
        super().__init__(config, uid)
        self.outcoming_queue = outcoming_queue
        self.last_action = time.time()

    def at_parser(self):
        """ Парсер доступных переходов. Собирается из разных полей стейта, зависит от конкретной реализации"""
        at = set()
        at.add(self.init_state)
        at.add(self.state)
        if "transitions" in self.current_state:
            at = at.union(set(self.current_state["transitions"]))

        if "data" in self.current_state:
            if "next" in self.current_state["data"]:
                at.add([self.transitions[self.state]["next"]])

            if "buttons" in self.current_state["data"]:
                for button in self.current_state["data"]["buttons"]:
                    at.add(button[1])

        self.logger.debug("Allowed transitions: {0}".format(at))
        return at

    def update(self, inp=None):
        self.last_action = time.time()

        state, data = self.parse_input(inp)
        self.action(state, data)

        return self

    def message(self, data):
        if "data" in self.current_state:
            msg = None
            keys = None

            if "text" in self.current_state["data"]:
                msg = self.current_state["data"]["text"]

            if "buttons" in self.current_state["data"]:
                keys = self.current_state["data"]["buttons"]
                self.logger.debug(keys)

            if (msg is not None) or (keys is not None):
                self.send(msg, keys)

            if "next" in self.current_state["data"]:
                self.update(self.current_state["data"]["next"] + " {}".format(data))

    def parse_input(self, inp):
        state = None
        data = inp
        if inp is not None:
            result = re.findall(COMMAND_PATTERN, inp, re.MULTILINE | re.IGNORECASE)
            result = list(filter(None, result))
            if len(result) and result[0] in self.transitions:
                state = result.pop(0)
                data = " ".join(result) if len(result) else None
        return state, data

    def send(self, msg, keys=None):
        result = {"chat_id": self.uid, "text": msg}
        if keys is not None:
            result["reply_markup"] = self.inline(keys)
        self.outcoming_queue.put(result)

    def inline(self, buttons):
        arr = []
        for button in buttons:
            key = {"text": button[0]}
            if ("http" in button[1]) or ("www." in button[1]):
                key["url"] = button[1]
            else:
                key["callback_data"] = button[1]
            arr.append([key])
        keys = json.dumps({"inline_keyboard": arr})
        return keys
