import unittest
import logging
import sys
from fsm.tests.fsm_test import FSMBaseTest
from fsm.fsm_base import HandlerDoesNotExistError, BadConfigError, TransitionDoesNotAllowedError
from fsm.tests.fsm_test_config import fsm as config


class TestFSMBase(unittest.TestCase):

    def setUp(self):
        self.config = config
        self.fsm = FSMBaseTest(config, 1)

    def test_new_user_starts(self):
        """Проверяем, что начало сессии происходит корректно - изначано установлен базовый стейт(/start),
        повторный его вызов приводит к обработке конфига (т.е. к отправке сообщения)"""

        self.assertEqual(self.fsm.state, "/start")
        self.assertEqual(len(self.fsm.msgs), 0)

        self.fsm.action("/start", None)
        self.assertEqual(self.fsm.msgs[-1], "Hello new user!")

    def test_states_flow(self):
        """Проверяем переходы на разрешенные и неразрашенные стейты"""
        self.assertEqual(self.fsm.state, "/start")

        self.fsm.action("state1")
        self.assertEqual(self.fsm.state, "state1")

        self.fsm.action("/start")
        self.assertEqual(self.fsm.state, "/start")

        self.fsm.action("state1")
        self.assertEqual(self.fsm.state, "state1")

        self.fsm.action("state2")
        self.assertEqual(self.fsm.state, "state2")

        self.fsm.action("state3")
        self.assertEqual(self.fsm.state, "state3")

        self.fsm.action("state2")
        self.assertEqual(self.fsm.state, "state2")

        self.assertRaises(TransitionDoesNotAllowedError, self.fsm.action, "state1")
        self.assertEqual(self.fsm.state, "state2")

        self.fsm.action("state3")
        self.assertEqual(self.fsm.state, "state3")

        self.fsm.action("/final")
        self.assertEqual(self.fsm.state, "/final")

    def test_allowed_transitions(self):
        """Проверяем корректность определения разрешенных переходов"""
        self.assertEqual(self.fsm.state, "/start")
        self.fsm.action("message_test")
        at = self.fsm.at_parser()
        self.assertEqual(at, {self.fsm.init_state, "/final", "next_test", "message_test"})

        self.fsm.action("next_test")
        at = self.fsm.at_parser()
        self.assertEqual(at, {self.fsm.init_state, "/final"})

    def test_next(self):
        """Тестирование автоматического перехода по next"""
        self.assertEqual(self.fsm.state, "/start")
        self.fsm.action("next_test")
        self.assertEqual(self.fsm.state, "/final")

    def test_send_message(self):
        """Тестирование отправки сообщения"""
        self.assertEqual(self.fsm.state, "/start")

        self.fsm.action("message_test")
        self.assertEqual(self.fsm.state, "message_test")
        self.assertEqual(self.fsm.msgs[-1], "Test message")

    def test_send_message_next(self):
        """Тестирование отправки сообщения и автоматического перехода по next"""
        self.assertEqual(self.fsm.state, "/start")

        self.fsm.action("message_next_test")
        self.assertEqual(self.fsm.state, "/final")
        self.assertEqual(self.fsm.msgs[-1], "Test message next")

    def test_handler(self):
        """Тестирование хендлеров"""
        self.assertEqual(self.fsm.state, "/start")
        self.fsm.action("cmd_state")
        self.assertEqual(self.fsm.state, "cmd_state")

        self.fsm.action("cmd_state_next")
        self.assertEqual(self.fsm.state, "/final")

    def test_handler_doesnotexists(self):
        """Кейс с отсутствующим хендлером"""
        self.assertEqual(self.fsm.state, "/start")
        self.assertRaises(HandlerDoesNotExistError, self.fsm.action, "handler_does_not_exists")

    def test_state_doesnotexists(self):
        """Кейс с отсутствующим в конфиге стейтом"""
        self.assertEqual(self.fsm.state, "/start")
        self.assertRaises(BadConfigError, self.fsm.action, "state_does_not_exists")



if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format='%(asctime)-12s : %(name)-15s : %(levelname)-8s %(message)s')
    unittest.main()
