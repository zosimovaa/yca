import unittest
import logging
import sys
from fsm.tests.fsm_test import FSMTelegramTest
from fsm.tests.fsm_test_config import fsm as config

class TestFSMTelegram(unittest.TestCase):

    def setUp(self):

        self.fsm = FSMTelegramTest(config, 1, [])

    def test_command_parser_1(self):
        """На входе значение из одного слова. Если есть в транзишинах - значит команда. Иначе - данные"""
        cmd, data = self.fsm.parse_input("/start")
        self.assertEqual(cmd, "/start")
        self.assertEqual(data, None)

    def test_command_parser_2(self):
        """На входе значение из команды и доп параметра. Если первое слово есть в транзишинах - значит команда. Иначе - данные"""
        cmd, data = self.fsm.parse_input("/start new user")
        self.assertEqual(cmd, "/start")
        self.assertEqual(data, "new user")

    def test_command_parser_3(self):
        """На входе значение не из транзишинов - т.е. данные"""
        cmd, data = self.fsm.parse_input("start")
        self.assertEqual(cmd, None)
        self.assertEqual(data, "start")

    def test_command_parser_4(self):
        """На входе значение не из транзишинов - т.е. данные, несколько слов"""
        cmd, data = self.fsm.parse_input("start new user")
        self.assertEqual(cmd, None)
        self.assertEqual(data, "start new user")

if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format='%(asctime)-12s : %(name)-15s : %(levelname)-8s %(message)s')
    unittest.main()
