from fsm.fsm_base import FSMBase
from fsm.fsm_telegram import FSMTelegram


class FSMBaseTest(FSMBase):
    def __init__(self, config, chat_id):
        super().__init__(config, chat_id)
        self.msgs = []
        return

    def send(self, msg):
        self.msgs.append(msg)

    def cmd_state(self, data=None):
        self.send(data)

    def cmd_state_next(self, data=None):
        self.action("/final")


class FSMTelegramTest(FSMTelegram):
    def __init__(self, config, chat_id, msgs_queue):
        super().__init__(config, chat_id, msgs_queue)
        self.msgs = msgs_queue
        self.keys = []
        return

    def send(self, msg, keys=None):
        self.msgs.append(msg)
        self.keys.append(keys)

    def cmd_state(self, data=None):
        self.send(data)

    def cmd_state_next(self, data=None):
        self.update("/final")
