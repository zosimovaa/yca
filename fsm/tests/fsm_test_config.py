fsm = {
    "init_state": "/start",
    "transitions":{

        #test_new_user_starts
        "/start": {
            "desc": "Начальный стейт",
            "handler": "message",
            "data": {
                "text": "Hello new user!",
            },
            "transitions": ["state1", "message_test", "next_test", "message_next_test" , "cmd_state", "handler_does_not_exists", "state_does_not_exists"]
        },
        "/final": {
            "desc": " Финальный стейт, переход только в стейт инициализации",
        },


        #states_flow
        "state1": {
            "desc": "Descriptions",
            "transitions": ["state2"]
        },
        "state2": {
            "desc": "Descriptions",
            "transitions": ["state3"]
        },
        "state3": {
            "desc": "Descriptions",
            "transitions": ["state2", "/final"]
        },

        #message_handler
        "message_test": {
            "desc": "Descriptions",
            "handler": "message",
            "data": {
                "text": "Test message"
            },
            "transitions": ["/final", "next_test"]
        },
        "next_test": {
            "desc": "Descriptions",
            "handler": "message",
            "data": {
                "next": "/final"
            }
        },
        "message_next_test": {
            "desc": "Descriptions",
            "handler": "message",
            "data": {
                "text": "Test message next",
                "next": "/final"
            }
        },

        "cmd_state": {
            "desc": "Descriptions",
            "handler": "cmd_state",
            "transitions": ["/final", "cmd_state_next"]

        },
        "cmd_state_next": {
            "desc": "Descriptions",
            "handler": "cmd_state_next",
            "transitions": ["/final"]

        },
        "handler_does_not_exists": {
            "desc": "Descriptions",
            "handler": "handlerdoesnotexists",
            "transitions": ["/final"]

        }
    }
}