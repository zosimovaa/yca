import re
import logging
import pymorphy2
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans

class TCException(Exception):
    """Базовое исключение для Txt_Cluster"""
    def __init__(self, msg, e):
        super().__init__(msg)
        self.user_message = "Произошла ошибка при анализе комментариев. Повторите попытку позже."
        self.original_exception = e
        self.type = "warning"

class TxtCluster:
    """Класс предоставляет функционал кластеризации текста"""
    REPLACE_NO_SPACE = re.compile("[.;:!\'?,\"()\[\]]")

    def __init__(self):
        self.logger = logging.getLogger("TXT.Cluster")
        self.lemm = pymorphy2.MorphAnalyzer()
        self.vect = TfidfVectorizer()
        try:
            #with open("stopwords.txt", "a") as f:
            f = open("ml/stopwords.txt")
            self.stopwords = f.read().splitlines()
            self.logger.info("stopwords ok")
        except Exception as e:
            self.stopwords = []
            self.logger.warning(e, exc_info=True)



    def preproc(self, sentence):
        """Предобработка предложения - удаление символов, перевод в нижний регистр, перевод в массив,
        лемматизация"""
        if isinstance(sentence, str):
            sentence = TxtCluster.REPLACE_NO_SPACE.sub("", sentence)
            sentence = sentence.lower()
            sentence = self.prep_lemm(sentence)
        return sentence

    def prep_lemm(self, sentence):
        """Лемматизация, удаление стоп-слов"""
        data = sentence.split()
        res=[]
        for word in data:
            if word in self.stopwords: continue
            word = self.lemm.parse(word)  # stemmer.stem(word)
            res.append(word[0].normal_form)
        data = " ".join(res)
        return data



    def processing(self, data, clusters, words):
        """Кластеризация подготовленных данных"""
        try:
            X = data["comment"].apply(lambda x: self.preproc(x))

            X = self.vect.fit_transform(list(X.values))

            true_k = clusters
            model = KMeans(n_clusters=true_k, init='k-means++', max_iter=1000, n_init=1)
            model.fit(X)

            order_centroids = model.cluster_centers_.argsort()[:, ::-1]
            terms = self.vect.get_feature_names()

            msg = ""
            for i in range(true_k):
                #print("Cluster %d:" % i),
                msg = msg + "\nCluster {0}:".format(i)
                for ind in order_centroids[i, :words]:
                    #print(' %s' % terms[ind]),
                    msg = msg + " {0}, ".format(terms[ind])
            return msg
        except Exception as e:
            raise TCException(e.__str__(), e)
