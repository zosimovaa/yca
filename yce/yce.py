import re
import logging
import requests
import pandas as pd


class YCEException(Exception):
    """Базовое исключение для YoutubeCommentExtractor"""
    def __init__(self, msg, vid):
        super().__init__(msg)
        self.vid = vid
        self.user_message = "Произошла ошибка при получении комментариев с ютуба. Повторите попытку позже."
        self.type = "warning"


class YCERequestError(YCEException):
    """Ошибка при выполнении запроса или парсинге ответа в JSON"""
    def __init__(self, e, vid):
        super().__init__(e.__str__(), vid)
        self.original_exception = e


class YCEResponseError(YCEException):
    """Ошибка в ответе ютуба"""
    def __init__(self, msg, vid, code):
        super().__init__(msg, vid)
        self.code = code
        self.type = "critical"


class YCEParsingError(YCEException):
    """Не обнаруженые искомые ключи"""
    def __init__(self, e, vid):
        super().__init__(e.__str__(), vid)
        self.original_exception = e
        self.type = "critical"


class YoutubeCommentExtractor:
    """ Youtube comment extractor -  подключается к апи ютуба и скачивает заданное количество комментариев к видео.
    В качестве входного параметра принимает id видео """
    REPLACE_NO_SPACE = re.compile("[.;:!\'?,\"()\[\]]", re.IGNORECASE)
    REPLACE_WITH_SPACE = re.compile("[\r\n]", re.IGNORECASE)
    RESULTS_PER_PAGE = 100       # Количество результатов на страницу в ответе youtube api

    def __init__(self, config):
        self.logger = logging.getLogger('YCE')
        try:
            self.key = config["api_key"]
            self.api_url = config["api_url"]
            self.max_res = config["result_per_page"]

        except KeyError as e:
            raise e

        self.comments = []
        self.vid_id = None

    def __get_comments(self, next_page_token=None):
        self.logger.debug("Запрос комментариев для видео {0}, next_page_token: {1}".format(self.vid_id, next_page_token))
        out = []
        new_next_page_token = None
        api_url = str(self.api_url).format(self.key, self.vid_id, self.max_res)

        if next_page_token is not None:
            api_url = api_url+"&pageToken={0}".format(next_page_token)

        try:
            r = requests.get(api_url)
            resp = r.json()
        except Exception as e:
            raise YCERequestError(e, self.vid_id)

        if "error" in resp:
            self.logger.debug(resp)
            raise YCEResponseError(resp["error"]["message"], self.vid_id, resp["error"]["code"])

        try:
            for i in range(len(resp["items"])):
                text = resp["items"][i]["snippet"]["topLevelComment"]["snippet"]["textOriginal"]
                text = self.REPLACE_NO_SPACE.sub("", text)
                text = self.REPLACE_WITH_SPACE.sub(" ", text)

                out.append([
                    resp["items"][i]["id"],
                    resp["items"][i]["snippet"]["topLevelComment"]["snippet"]["videoId"],
                    resp["items"][i]["snippet"]["topLevelComment"]["snippet"]["authorDisplayName"],
                    resp["items"][i]["snippet"]["topLevelComment"]["snippet"]["publishedAt"],
                    text,
                    resp["items"][i]["snippet"]["topLevelComment"]["snippet"]["likeCount"]
                ])
        except KeyError as e:
            raise YCEParsingError(e, self.vid_id)

        except Exception as e:
            raise YCEException(e.__str__(), self.vid_id)

        if "nextPageToken" in resp:
            new_next_page_token = resp["nextPageToken"]
        return out, new_next_page_token

    def get_video_comments(self, vid_id, count_cut_off=None):
        self.logger.debug("Запрос комментариев для видео {}".format(vid_id))
        self.vid_id = vid_id
        self.comments = []
        collected = 0
        do_collect = True
        next_page_token = None
        while do_collect:
            delta = count_cut_off - collected
            data, next_page_token = self.__get_comments(next_page_token)

            self.comments = self.comments + data[:delta]
            collected = collected + len(data[:delta])

            if collected >= count_cut_off:
                do_collect = False

            if next_page_token is None:
                do_collect = False
        self.logger.debug("Собрано комментариев: {}".format(collected))
        self.comments = pd.DataFrame(self.comments, columns=["thread_id", "video_id", "name", "date", "comment", "likes"])
        return self.comments

