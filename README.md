# Youtube Comments Analyzer
Телеграм бот, анализирующий комментарии к видео на youtube.com


### Общее описание
В главном приложении создаются объекты телеграм-бота и сессионного менеджера.
Они обмениваются сообщениями через 2 очереди - по одной сообщения из телеграма передаются в сессионный менеджер для обработки, вторая очередь служит для отправки сообщений.

### Telegram
Реализовано в виде 2-х сервисов - "слушатель" сообщений и "отправитель" сообщений. 
 - "Слушатель" забирает обновления с сервера телеграма и складывает из в **очередь заданий**. 
 - "Отправитель" забирает сообщения из **очереди сообщений**. Можно запустить несколько процессов для распараллеливания отправки.

### Менеджер сессий и исполнители
 - Менеджер сессий исполняет несколько функций - хранит сессии и удаляет старые, запускает процессы исполнители (количество регулируется). 
 - Процесс-исполнитель вычитывает из **очереди заданий** сообщения, полученные с сервера телеграма и передает их в стейт-машину, которая является объектом-сессией.

### Конечный автомат
##### Базовая версия
Предоставляет минимальный функционал - переход по стейтам с возможностью передави данных в оработчики

##### Для телеграма
Дополнительно реализован парсер команд - строка разделяется по словам, первое слово проверяется на вхождение в стейт-машину. Если соответствует одному из стейов - то воспринимается как команда, оставшая часть строки - как данные. В противном случае вся строка воспринимается как данные.
##### Для анализатора комментариев с ютуба
Реализованы обработчики стейтов

### Экстрактор комментариев с ютуба


### ML Модуль
