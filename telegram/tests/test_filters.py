import unittest
from telegram.filters import youtube_link


class TestMessageFilters(unittest.TestCase):

    def setUp(self):
        self.youtube_link = youtube_link

    def test_youtube(self):
        self.assertEqual(self.youtube_link("https://youtu.be/IUfuc2fLcPo"), "youtu IUfuc2fLcPo")
        self.assertEqual(self.youtube_link("https://www.youtube.com/watch?v=-MFKGPVnt1Y"), "youtube -MFKGPVnt1Y")
        self.assertEqual(self.youtube_link("/start"), "/start")
        self.assertEqual(self.youtube_link("start"), "start")
        self.assertEqual(self.youtube_link("10"), "10")
        self.assertEqual(self.youtube_link("просто какое кто сообщение со ссылкой https://youtu.be/IUfuc2fLcPo"),
                         "youtu IUfuc2fLcPo")
        self.assertEqual(self.youtube_link("просто какое кто сообщение со ссылкой https://ya.ru"),
                         "просто какое кто сообщение со ссылкой https://ya.ru")
        self.assertEqual(self.youtube_link("просто какое кто сообщениебез ссылки"),
                         "просто какое кто сообщениебез ссылки")
        self.assertEqual(self.youtube_link("https://youtu.be/IUfuc2fLcPo?оаоаооокок"),
                         "youtu IUfuc2fLcPo")
        self.assertEqual(self.youtube_link(""), "")
        self.assertEqual(self.youtube_link([1,2,3]), [1,2,3])
        self.assertEqual(self.youtube_link("\n"), "\n")


if __name__ == '__main__':
    unittest.main()
