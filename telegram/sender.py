import logging
import requests
import threading


class Sender(threading.Thread):
    """Отправка в отдельном потоке сообщений из очереди в телеграм"""

    def __init__(self, outcoming_queue, api_url):
        super().__init__()
        self.logger = logging.getLogger('TLG.SEND')
        self.outcoming_queue = outcoming_queue
        self.api_url = api_url
        self.logger.debug("Sender initialized")

    def run(self):
        self.logger.debug("Sender thread started")
        #headers = {"Content-type": "application/json"}
        method = "sendMessage"
        while True:
            try:
                params = self.outcoming_queue.get(block=True)
                res = requests.post(self.api_url + method, data=params)
                response = res.json()
                self.outcoming_queue.task_done()
                if response["ok"]:
                    self.logger.info("Message was sent. Params: {}".format(params))
                else:
                    self.logger.warning("Message does not sent. Params: {}".format(params))

            except Exception as e:
                self.logger.error(e, exc_info=True)
                #TODO проработать обработку ошибок


"""
{'ok': True, 'result': {'message_id': 1783, 'from': {'id': 481000054, 'is_bot': True, 'first_name': 'Informator', 'username': 'GoodInformer_bot'}, 'chat': {'id': 211945135, 'first_name': 'Aleksei', 'last_name': 'Zosimov', 'username': 'lesha_spb', 'type': 'private'}, 'date': 1556636320, 'text': 'Test message for send'}}
"""