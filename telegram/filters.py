import re
import logging

YOUTUBE_PATTERN = "(youtube|youtu)|(?:watch\?v=)([^&=\n%\?]{11})|(?:be\/)([^&=\n%\?]{11})"


def youtube_link(msg):
    """Если входная строка является ссылкой на видео youtube, то в результате будет возвращена строка
        вида '<команда> <данные>'. Где в качествек команды будет значение 'youtube' (или 'youtu'),
        а в качестве данных -  id видео"""
    logger = logging.getLogger('FLTR.youtube_link')
    res = msg
    logger.debug("Input: {}".format(res))
    if msg is not None and isinstance(msg, str):
        result = re.findall(YOUTUBE_PATTERN, msg, re.MULTILINE | re.IGNORECASE)
        if len(result) >= 2:
            cmd = [value for value in result[0] if value]
            dt = [value for value in result[1] if value]
            if len(cmd) >= 1 and len(dt) >= 1:
                res = cmd[0] + ' ' + dt[0]
    logger.debug("Output: {}".format(res))
    return res


filters = [youtube_link]
