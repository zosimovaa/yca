import time
import requests
import threading
import logging
from telegram.sender import Sender


class Telegram(threading.Thread):
    """Класс предоставляет фукнкциональность получения и отправки сообщений в телеграм"""

    def __init__(self, config, outcoming_queue, incoming_queue, filters):
        threading.Thread.__init__(self)
        self.logger = logging.getLogger('TLG.POLL')

        try:
            self.module_cfg = config["bot"]
            self.token = self.module_cfg["token"]
            self.sender_threads = self.module_cfg["sender_threads"]
            self.polling_timeout = self.module_cfg["polling_timeout"]
        except KeyError as e:
            #Todo  Продумать обработку ошибок при кривом конфиге. Пока так.
            raise e

        self.incoming_queue = incoming_queue
        self.outcoming_queue = outcoming_queue
        self.filters = filters
        self.api_url = "https://api.telegram.org/bot{0}/".format(self.token)
        self.offset = 0
        self.send_threads = []

        for i in range(self.sender_threads):
            t = Sender(self.outcoming_queue, self.api_url)
            t.start()
            self.send_threads.append(t)

        self.logger.debug("Bot initialized")

    def run(self):
        self.get_updates(0)
        while True:
            update = self.get_updates(self.polling_timeout)
            self.logger.debug(update)
            if len(update):
                for i in update:
                    self.income(i)

    def get_updates(self, timeout):
        method = "getUpdates"
        params = {"timeout": timeout, "offset": self.offset}
        data = {}
        self.logger.debug("Getting updates with params - {}".format(params))
        try:
            resp = requests.get(self.api_url + method, params)
            data = resp.json()["result"]                                #ToDo Сделать обработку ok=true
            if len(data):
                self.logger.debug(data)
                self.offset = data[-1]["update_id"] + 1
        except Exception as e:
            self.logger.error(e, exc_info=True)
            data = {}
            time.sleep(10)
        finally:
            return data

    def income(self, message):
        """ Точка приема сообщений. Переопределить метод для реализации функционала"""
        inp = None
        chat_id = 0
        if "message" in message:
            self.logger.debug("Income message handler")
            inp = message["message"]["text"]
            chat_id = message["message"]["chat"]["id"]
        elif "callback_query" in message:
            self.logger.debug("Income callback_query handler")
            inp = message["callback_query"]["data"]
            chat_id = message["callback_query"]["message"]["chat"]["id"]
        else:
            self.logger.warning("Unknown message type (message or callback_query does not exists)")
            self.logger.warning(message)

        for f in self.filters:
            inp = f(inp)

        self.incoming_queue.put({
            "id": chat_id,
            "data": inp
        })

    def send(self, chat_id, message):
        self.outcoming_queue.put({"chat_id": chat_id, "text": message})


